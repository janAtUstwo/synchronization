//
//  Batch.swift
//  Emoji Sudoku
//
//  Created by Jan Mazurczak on 18/09/2019.
//  Copyright © 2019 Jan's Games. All rights reserved.
//

import Foundation

public class Batch<Item> {
    
    private let syncQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    public private(set) var items = [Item]()
    public private(set) var interval: TimeInterval
    public private(set) var limit: Int?
    public private(set) var execution: ([Item]) -> Void
    
    public init(for itemType: Item.Type, interval: TimeInterval, limit: Int?, execution: @escaping ([Item]) -> Void) {
        self.interval = interval
        self.limit = limit
        self.execution = execution
    }
    
    public func set(interval: TimeInterval) {
        syncQueue.addOperation {
            self.interval = interval
            self.scheduleExecution()
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    public func set(limit: Int?) {
        syncQueue.addOperation {
            self.limit = limit
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    public func set(execution: @escaping ([Item]) -> Void) {
        syncQueue.addOperation {
            self.execution = execution
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    public func filter(_ include: @escaping (Item) -> Bool) {
        syncQueue.addOperation {
            self.items = self.items.filter(include)
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    public func add(_ item: Item) {
        add([item])
    }
    
    public func add(_ items: [Item]) {
        syncQueue.addOperation {
            if items.isEmpty { return }
            let startCounting = self.items.isEmpty
            self.items.append(contentsOf: items)
            if startCounting {
                self.scheduleExecution()
            }
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
    private var scheduledExecutionID: UUID?
    private func scheduleExecution() {
        let seid = UUID()
        scheduledExecutionID = seid
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(interval * 1000))
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            // self is intentionally retained in block to keep Batch alive until executed
            self.syncQueue.addOperation {
                guard self.scheduledExecutionID == seid else { return }
                self.execute()
            }
        }
    }
    
    private func execute() {
        if items.isEmpty { return }
        
        let convictsCount = min(items.count, limit ?? items.count)
        let convicts = Array(items.prefix(convictsCount))
        
        items.removeFirst(convictsCount)
        OperationQueue.main.addOperation {
            self.execution(convicts)
        }
        
        if !items.isEmpty {
            scheduleExecution()
        }
    }
    
}

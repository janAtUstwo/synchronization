//
//  SynchronizationCloudKit.swift
//
//  Created by Jan Mazurczak on 30/08/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import CloudKit
import Synchronization
import Batch
import Combine

public class SynchronizationCloudKit: SynchronizationRemote {
    
    public enum RecordTypeName {
        case common
        case coordinatorID
        case custom(String)
        func value(in coordinatorID: String) -> CKRecord.RecordType {
            switch self {
            case .common: return "SyncItem"
            case .coordinatorID: return coordinatorID
            /// Use to keep old records after changing type name if itemType was used before
            case .custom(let name): return name
            }
        }
    }
    
    public enum RecordIdentifier {
        /// Identifiers must be unique between coordinators
        case original
        /// Identifiers will be automatically uniqued between coordinators by adding coordinator identifier as a prefix
        case prefixedWithCoordinator
        /// Provide custom mapping for remote record identifier. Local identifier is a mapper input
        case custom((_ coordinatorID: String, _ itemID: String) -> String)
        func value(for identifier: String, in coordinatorID: String) -> CKRecord.ID {
            let name: String
            switch self {
            case .original: name = identifier
            case .prefixedWithCoordinator: name = coordinatorID + identifier
            case .custom(let mapper): name = mapper(coordinatorID, identifier)
            }
            return CKRecord.ID(recordName: name.filenameSafe())
        }
    }
    
    private let recordTypeName: RecordTypeName
    private let recordIdentifier: RecordIdentifier
    private let database: CKDatabase
    private let remoteOperationQueue: OperationQueue
    private let logType: CKRecord.RecordType = "SyncLog"
    
    private struct PushCompletionableItem {
        /// id defines for which record completion should be fired.
        let id: CKRecord.ID
        let add: [CKRecord]
        let delete: [CKRecord.ID]?
        let completion: (CKRecord, Error?) -> Void
    }
    private let pushBatch: Batch<PushCompletionableItem>
    
    private struct FetchCompletionableItem {
        let id: CKRecord.ID
        let completion: (CKRecord?, Error?) -> Void
    }
    private let fetchBatch: Batch<FetchCompletionableItem>
    
    private enum SubscriptionRequest {
        case create(CKSubscription)
        case delete(CKSubscription.ID)
    }
    private let subscribeBatch: Batch<SubscriptionRequest>
    
    public static let defaultName = "CloudKit"
    public let name: String
    
    public init(name: String = SynchronizationCloudKit.defaultName, recordTypeName: RecordTypeName = .common, recordIdentifier: RecordIdentifier = .prefixedWithCoordinator, database: CKDatabase = CKContainer.default().privateCloudDatabase) {
        self.name = name
        self.recordTypeName = recordTypeName
        self.recordIdentifier = recordIdentifier
        self.database = database
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        self.remoteOperationQueue = queue
        self.pushBatch = Batch(for: PushCompletionableItem.self, interval: 1.5, limit: 50) { pushingItems in
            Logger.log?("Pushing batch: " + pushingItems.map({ $0.id.recordName }).joined(separator: ", "))
            let records = pushingItems.flatMap { $0.add }
            let recordsToRemove = pushingItems.flatMap { $0.delete ?? [] }
            let operation = CKModifyRecordsOperation(recordsToSave: records, recordIDsToDelete: recordsToRemove.isEmpty ? nil : recordsToRemove)
            operation.isAtomic = false
            operation.database = database
            operation.perRecordCompletionBlock = { record, error in
                if let error = error { Logger.log?("Push error: \(error)") }
                pushingItems.last(where: { $0.id == record.recordID })?.completion(record, error)
            }
            queue.addOperation(operation)
        }
        self.fetchBatch = Batch(for: FetchCompletionableItem.self, interval: 1.2, limit: 10) { fetchingItems in
            Logger.log?("Fetching batch: " + fetchingItems.map({ $0.id.recordName }).joined(separator: ", "))
            let operation = CKFetchRecordsOperation(recordIDs: fetchingItems.map { $0.id })
            operation.database = database
            operation.perRecordCompletionBlock = { record, recordID, error in
                if let error = error { Logger.log?("Fetch error: \(error)") }
                fetchingItems.last(where: { $0.id == recordID })?.completion(record, error)
            }
            queue.addOperation(operation)
        }
        self.subscribeBatch = Batch(for: SubscriptionRequest.self, interval: 0.5, limit: 5) { requests in
            let create: [CKSubscription] = requests.compactMap {
                switch $0 {
                case .create(let s): return s
                case .delete: return nil
                }
            }
            let delete: [CKSubscription.ID] = requests.compactMap {
                switch $0 {
                case .create: return nil
                case .delete(let s): return s
                }
            }
            let operation = CKModifySubscriptionsOperation(subscriptionsToSave: create, subscriptionIDsToDelete: delete)
            operation.database = database
            queue.addOperation(operation)
        }
    }
    
    public func fetch<Item>(_ itemType: Item.Type, identifier: String, coordinatorID: String, modelVersion: Int, with remoteRuntimeCache: Any?, completion: @escaping (SynchronizationRemoteFetchResult<Item>) -> Void) {
        let recordID = recordIdentifier.value(for: identifier, in: coordinatorID)
        fetchBatch.filter { $0.id != recordID }
        fetchBatch.add(FetchCompletionableItem(id: recordID, completion: { record, error in
            guard let record = record else {
                if (error as? CKError)?.code == CKError.Code.unknownItem {
                    completion(SynchronizationRemoteFetchResult(
                        status: .nothingToFetch,
                        item: nil,
                        runtimeCache: .changed(nil)))
                } else {
                    completion(SynchronizationRemoteFetchResult(
                        status: .fetchingFailed,
                        item: nil,
                        runtimeCache: .unchanged))
                }
                return
            }
            if
                let itemDataURL = (record["itemData"] as? CKAsset)?.fileURL,
                let itemData = try? Data(contentsOf: itemDataURL),
                let item = try? JSONDecoder().decode(Synchronizable<Item>.self, from: itemData)
            {
                completion(SynchronizationRemoteFetchResult(
                    status: .fetched,
                    item: item,
                    runtimeCache: .changed(record)))
            } else if
                let remoteModelVersion = record["modelVersion"] as? Int,
                remoteModelVersion > modelVersion
            {
                completion(SynchronizationRemoteFetchResult(
                    status: .tooNewToParse,
                    item: nil,
                    runtimeCache: .changed(record)))
            } else {
                completion(SynchronizationRemoteFetchResult(
                    status: .canNotParse,
                    item: nil,
                    runtimeCache: .changed(record)))
            }
        }))
    }
    
    public func push<Item>(item: Synchronizable<Item>, coordinatorID: String, with remoteRuntimeCache: Any?, completion: @escaping (SynchronizationRemotePushResult) -> Void) {
        let fileURL = URL.assetsCache.appendingPathComponent(UUID().uuidString)
        
        do {
            let itemData = try JSONEncoder().encode(item)
            try itemData.write(to: fileURL)
        } catch {
            completion(SynchronizationRemotePushResult(
                status: .encodingFailure,
                runtimeCache: .unchanged))
            return
        }
        
        let recordToPushIdentifier = recordIdentifier.value(for: item.identifier, in: coordinatorID)
        
        let recordToPush: CKRecord
        var oldLogs = [CKRecord.ID]()
        if let previousRecord = remoteRuntimeCache as? CKRecord {
            recordToPush = previousRecord
            if let oldLog = (previousRecord["lastLog"] as? CKRecord.Reference)?.recordID {
                oldLogs.append(oldLog)
            }
        } else {
            recordToPush = CKRecord(recordType: recordTypeName.value(in: coordinatorID), recordID: recordToPushIdentifier)
        }
        
        recordToPush["uuid"] = item.uuid.uuidString
        recordToPush["modelVersion"] = item.modelVersion
        recordToPush["itemModificationDate"] = item.modificationDate
        recordToPush["itemData"] = CKAsset(fileURL: fileURL)
        
        let recordLog = CKRecord(recordType: logType)
        recordLog["it"] = CKRecord.Reference(recordID: recordToPushIdentifier, action: .deleteSelf)
        recordLog["id"] = item.identifier
        recordLog["sc"] = coordinatorID
        
        recordToPush["lastLog"] = CKRecord.Reference(record: recordLog, action: .none)
        
        pushBatch.filter { $0.id != recordToPushIdentifier }
        pushBatch.add(PushCompletionableItem(id: recordToPushIdentifier, add: [recordToPush, recordLog], delete: oldLogs, completion: { record, error in
            try? FileManager.default.removeItem(at: fileURL)
            if let error = error {
                if (error as? CKError)?.code == CKError.Code.serverRecordChanged {
                    completion(SynchronizationRemotePushResult(
                        status: .failureRefetchIsNeeded,
                        runtimeCache: .unchanged))
                } else {
                    completion(SynchronizationRemotePushResult(
                        status: .failure,
                        runtimeCache: .unchanged))
                }
                return
            }
            completion(SynchronizationRemotePushResult(
                status: .success,
                runtimeCache: .changed(record)))
        }))
    }
    
    public func subscribeCoordinator(with identifier: String) {
        let subscription = CKQuerySubscription(
            recordType: logType,
            predicate: NSPredicate(format: "sc == %@", identifier),
            subscriptionID: identifier,
            options: CKQuerySubscription.Options.firesOnRecordCreation)
        subscription.notificationInfo = CKSubscription.NotificationInfo(desiredKeys: ["sc", "id"], shouldSendContentAvailable: true)
        subscribeBatch.add(.create(subscription))
    }
    
    public func unsubscribeCoordinator(with identifier: String) {
        subscribeBatch.add(.delete(identifier))
    }
    
    public func handle(notification userInfo: [AnyHashable : Any], completion: @escaping (Bool) -> Void) {
        guard
            let notification = CKQueryNotification(fromRemoteNotificationDictionary: userInfo),
            let coordinator = notification.recordFields?["sc"] as? String,
            let item = notification.recordFields?["id"] as? String
        else {
            Logger.log?("🛑 Failed to parse CloudKit notification")
            completion(false)
            return
        }
        Synchronization.handle(remotePush: RemotePushUpdate(
                                remoteName: name,
                                coordinatorID: coordinator,
                                item: item,
                                completion: completion))
    }
    
}

private extension URL {
    
    static let assetsCache: URL = {
        let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!.appendingPathComponent("Assets", isDirectory: true)
        try? FileManager.default.removeItem(at: url)
        try! FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        return url
    }()
    
}

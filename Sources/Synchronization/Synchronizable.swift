//
//  Synchronized.swift
//  
//
//  Created by Jan Mazurczak on 06/11/2019.
//

import Foundation

public struct Synchronizable<Item>: Codable, Equatable where Item: Codable {
    public let identifier: String
    public let uuid: UUID
    public let modelVersion: Int
    public private(set) var modificationDate: Date
    public private(set) var item: Item?
    
    internal init(identifier: String, uuid: UUID, modelVersion: Int, modificationDate: Date, item: Item?) {
        self.identifier = identifier
        self.uuid = uuid
        self.modelVersion = modelVersion
        self.modificationDate = modificationDate
        self.item = item
    }
    
    public enum Merged {
        case selected(Synchronizable<Item>)
        case mixed(Item?, Date)
        case none
    }
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.uuid == rhs.uuid
    }
}

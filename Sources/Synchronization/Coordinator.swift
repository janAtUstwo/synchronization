//
//  Coordinator.swift
//  
//
//  Created by Jan Mazurczak on 06/11/2019.
//

import Foundation
import OnDemandDictionary
import Combine
import SwiftUI

internal protocol AnySynchronizationCoordinator: class {
    var identifier: String { get }
    func synchronizeFromBacklog()
    func fetchCacheAndRelease(item identifier: String, completion: ((Bool) -> Void)?)
}

public class SynchronizationCoordinator<Item>: AnySynchronizationCoordinator where Item: Codable {
    
    // public
    
    public let identifier: String
    public let url: URL
    internal let remotes: [SynchronizationRemoteWithBacklog]
    internal var settings: SynchronizationSettings<Item>
    
    /// Retrieves existing synchronizer or creates new one. Synchronizer returned has no strong reference so make sure to keep strong reference to it as long as needed and wherever needed. You can use this method multiple times to retrieve the same instance of the synchronizer as long as it is strongly referenced somewhere else.
    public func synchronizer(_ identifier: String) -> Synchronizer<Item> {
        return synchronizers.retrieveItem(for: identifier) { Synchronizer(identifier: $0, coordinator: self) }
    }
    
    public func synchronizeFromBacklog() {
        for remote in remotes {
            remote.synchronizeFromBacklog(with: self)
        }
    }
    
    public func fetchCacheAndRelease(item identifier: String, completion: ((Bool) -> Void)?) {
        synchronizer(identifier).fetch(completion: completion)
    }
    
    // private
    
    internal init(identifier: String, url: URL, remotes: [SynchronizationRemote], settings: SynchronizationSettings<Item>, pushUpdatePublisher: AnyPublisher<ExecutingRemotePushUpdate, Never>) {
        try! FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        
        self.identifier = identifier
        self.url = url
        self.remotes = remotes.map { SynchronizationRemoteWithBacklog(remote: $0, backlogContainer: url) }
        self.settings = settings
        
        for remote in remotes {
            remote.subscribeCoordinator(with: identifier)
        }
        
        pushUpdatePublisher
            .filter { $0.update.coordinatorID == identifier }
            .sink { [weak self] in self?.handle(remoteUpdate: $0) }
            .store(in: &subscriptions)
        
        NotificationCenter.default
            .publisher(for: UIApplication.didBecomeActiveNotification)
            .map { _ in }
            .sink { [weak self] in self?.synchronizeFromBacklog() }
            .store(in: &subscriptions)
    }
    
    internal func remote(with name: String) -> SynchronizationRemoteWithBacklog? {
        return remotes.first(where: { $0.remote.name == name })
    }
    
    private func handle(remoteUpdate: ExecutingRemotePushUpdate) {
        remoteUpdate.takeCare()
        let update = remoteUpdate.update
        synchronizer(update.item).fetch(from: update.remoteName, completion: update.completion)
    }
    
    private let synchronizers = WeakOnDemandDictionary<String, Synchronizer<Item>>()
    private var subscriptions = Set<AnyCancellable>()
    
}

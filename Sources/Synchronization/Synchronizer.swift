//
//  Synchronizer.swift
//  
//
//  Created by Jan Mazurczak on 06/11/2019.
//

import Foundation
import Combine

public class Synchronizer<Item>: ObservableObject where Item: Codable {
    
    public let identifier: String
    public var publisher: AnyPublisher<Item?, Never> { subject.eraseToAnyPublisher() }
    
    public var item: Item? {
        set { update(item: newValue, modified: Date()) }
        get { subject.value }
    }
    
    public func modify(at modified: Date = Date(), modification: @escaping (inout Item?) -> Void) {
        sync {
            Logger.log?("Synchronizer \(self.identifier) is updating")
            var item = self.merged.value?.item
            modification(&item)
            let syncItem = Synchronizable<Item>(identifier: self.identifier, uuid: UUID(), modelVersion: self.coordinator.settings.modelVersion, modificationDate: modified, item: item)
            
            self.local = syncItem
            self.save(syncItem)
            
            self.merge() // check if it needs to be modified merging with remote
            if self.isSaveNeeded {
                self.save()
            }
            for remote in self.pushExpectingRemotes {
                self.pushNow(to: remote, completion: nil)
            }
        }
    }
    
    public func update(item: Item?, modified: Date = Date()) {
        modify(at: modified) {
            $0 = item
        }
    }
    
    public func fetch(from remoteName: String? = nil, completion: ((Bool) -> Void)? = nil) {
        sync {
            let remotes: [Remote]
            if let remoteName = remoteName {
                guard let remote = self.remotes.first(where: { $0.name == remoteName }) else { return }
                remotes = [remote]
            } else {
                remotes = self.remotes
            }
            let group = DispatchGroup()
            var fetchSuccess = true
            for remote in remotes {
                group.enter()
                self.fetchNow(from: remote) { success in
                    if !success { fetchSuccess = false }
                    group.leave()
                }
            }
            group.notify(queue: DispatchQueue.main) {
                completion?(fetchSuccess)
            }
        }
    }
    
    public func push(to remoteName: String? = nil, completion: ((Bool) -> Void)? = nil) {
        sync {
            let remotes: [Remote]
            if let remoteName = remoteName {
                guard let remote = self.remotes.first(where: { $0.name == remoteName }) else { return }
                remotes = [remote]
            } else {
                remotes = self.remotes
            }
            let group = DispatchGroup()
            var pushSuccess = true
            for remote in remotes {
                group.enter()
                self.pushNow(to: remote) { success in
                    if !success { pushSuccess = false }
                    group.leave()
                }
            }
            group.notify(queue: DispatchQueue.main) {
                completion?(pushSuccess)
            }
        }
    }
    
    // private
    
    internal init(identifier: String, coordinator: SynchronizationCoordinator<Item>) {
        Logger.log?("Create Synchronizer \(identifier)")
        self.identifier = identifier
        self.coordinator = coordinator
        self.remotes = coordinator.remotes.map { Remote(name: $0.remote.name) }
        self.syncQueue = OperationQueue()
        self.syncQueue.maxConcurrentOperationCount = 1
        subject
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &subscriptions)
        sync {
            self.loadLocal()
        }
    }
    
    deinit {
        Logger.log?("Release Synchronizer \(identifier)")
    }
    
    internal var subject = CurrentValueSubject<Item?, Never>(nil)
    private var subscriptions = Set<AnyCancellable>()
    
    internal let coordinator: SynchronizationCoordinator<Item>
    internal let remotes: [Remote]
    internal var local: Synchronizable<Item>?
    internal var merged = CurrentValueSubject<Synchronizable<Item>?, Never>(nil)
    internal var localURL: URL {
        coordinator.url.appendingPathComponent(identifier.filenameSafe(), isDirectory: false)
    }
    
    internal let syncQueue: OperationQueue
    
}

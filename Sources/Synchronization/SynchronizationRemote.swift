//
//  SynchronizationRemote.swift
//
//  Created by Jan Mazurczak on 30/08/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation
import Combine

public enum SynchronizerRemoteStatus {
    case notFetchedYet
    case fetchingFailed
    case nothingToFetch
    case canNotParse
    case tooNewToParse
    case fetched
}

public struct SynchronizationRemoteFetchResult<Item> where Item: Codable {
    public let status: SynchronizerRemoteStatus
    public let item: Synchronizable<Item>?
    public let runtimeCache: RuntimeCache
    public init(status: SynchronizerRemoteStatus, item: Synchronizable<Item>?, runtimeCache: RuntimeCache) {
        self.status = status
        self.item = item
        self.runtimeCache = runtimeCache
    }
    public enum RuntimeCache {
        case unchanged
        case changed(Any?)
    }
}

public struct SynchronizationRemotePushResult {
    public let status: Status
    public let runtimeCache: RuntimeCache
    public init(status: Status, runtimeCache: RuntimeCache) {
        self.status = status
        self.runtimeCache = runtimeCache
    }
    public enum RuntimeCache {
        case unchanged
        case changed(Any?)
    }
    public enum Status {
        case success
        case failureRefetchIsNeeded
        case encodingFailure
        case failure
    }
}

public protocol SynchronizationRemote {
    var name: String { get }
    func fetch<Item>(_ itemType: Item.Type, identifier: String, coordinatorID: String, modelVersion: Int, with remoteRuntimeCache: Any?, completion: @escaping (SynchronizationRemoteFetchResult<Item>) -> Void)
    func push<Item>(item: Synchronizable<Item>, coordinatorID: String, with remoteRuntimeCache: Any?, completion: @escaping (SynchronizationRemotePushResult) -> Void)
    /// After calling this remote is responsible for calling Synchronization.handle(remotePush for specified coordinator updates on remote
    func subscribeCoordinator(with identifier: String)
    /// Use to remove subscriptions after migrating and removing any coordinators in future versions of the application
    func unsubscribeCoordinator(with identifier: String)
}

public struct RemotePushUpdate {
    public let remoteName: String?
    public let coordinatorID: String
    public let item: String
    public let completion: (Bool) -> Void
    public init(remoteName: String?, coordinatorID: String, item: String, completion: @escaping (Bool) -> Void) {
        self.remoteName = remoteName
        self.coordinatorID = coordinatorID
        self.item = item
        self.completion = completion
    }
}

public struct ExecutingRemotePushUpdate {
    public let update: RemotePushUpdate
    public let takeCare: () -> Void
    public init(_ update: RemotePushUpdate, takeCare: @escaping () -> Void) {
        self.update = update
        self.takeCare = takeCare
    }
}

internal class SynchronizationRemoteWithBacklog {
    let remote: SynchronizationRemote
    let backlog: PersistentIdentifiersStack
    var isSynchronizingFromBacklog = false
    
    init(remote: SynchronizationRemote, backlogContainer url: URL) {
        self.remote = remote
        self.backlog = PersistentIdentifiersStack(url: url.appendingBacklog.appendingPathComponent(remote.name, isDirectory: true), preferredItemsPerPage: 10, maxCachedPagesCount: 10)
    }
    
    func synchronizeFromBacklog(with coordinator: AnySynchronizationCoordinator) {
        Logger.log?("Check synchronization backlog \(coordinator.identifier)")
        if isSynchronizingFromBacklog { return }
        let identifiers = backlog.top(20).reversed()
        if identifiers.isEmpty { return }
        Logger.log?("Synchronize \(identifiers.count) backlog items \(coordinator.identifier)")
        isSynchronizingFromBacklog = true
        let group = DispatchGroup()
        identifiers.forEach {
            group.enter()
            coordinator.fetchCacheAndRelease(item: $0) { _ in
                group.leave()
            }
        }
        group.notify(queue: DispatchQueue.main) { [weak self] in
            Logger.log?("Synchronized backlog \(coordinator.identifier)")
            self?.isSynchronizingFromBacklog = false
            self?.synchronizeFromBacklog(with: coordinator)
        }
    }
}

internal extension URL {
    var appendingBacklog: URL { return appendingPathComponent("Backlog", isDirectory: true) }
}

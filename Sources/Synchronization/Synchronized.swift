//
//  Synchronized.swift
//  
//
//  Created by Jan Mazurczak on 29/07/2020.
//

import Foundation
import Combine

@propertyWrapper
public struct Synchronized<Item: Codable> {
    
    private let coordinator: SynchronizationCoordinator<Item>
    public private(set) var synchronizer: Synchronizer<Item>?
    
    public init(_ path: KeyPath<Synchronization, Synchronization.CoordinatorDefinition<Item>>, _ identifier: String? = nil) {
        coordinator = Synchronization.coordinator(path)
        synchronizer = identifier.map(coordinator.synchronizer)
        bindSynchronizer()
    }
    
    public var wrappedValue: Item? {
        get { synchronizer?.item }
        set { synchronizer?.item = newValue }
    }
    
    public var projectedValue: Self {
        get { self }
        set { self = newValue }
    }
    
    public var publisher: AnyPublisher<Item?, Never> { subject.eraseToAnyPublisher() }
    private var subject = CurrentValueSubject<Item?, Never>(nil)
    private var subjectBinding: AnyCancellable?
    private mutating func bindSynchronizer() {
        subjectBinding?.cancel()
        subjectBinding = synchronizer?.publisher
            .sink { [weak subject] in subject?.send($0) }
    }
    
    public var loadedIdentifier: String? { synchronizer?.identifier }
    public mutating func load(_ identifier: String?) {
        if let identifier = identifier {
            synchronizer = coordinator.synchronizer(identifier)
        } else {
            synchronizer = nil
        }
        bindSynchronizer()
    }
    
    public func modify(at modified: Date = Date(), modification: @escaping (inout Item?) -> Void) {
        synchronizer?.modify(at: modified, modification: modification)
    }
    
    public func update(item: Item?, modified: Date = Date()) {
        synchronizer?.update(item: item, modified: modified)
    }
    
    public func fetch(from remoteName: String? = nil, completion: ((Bool) -> Void)? = nil) {
        synchronizer?.fetch(from: remoteName, completion: completion)
    }
    
    public func push(to remoteName: String? = nil, completion: ((Bool) -> Void)? = nil) {
        synchronizer?.push(to: remoteName, completion: completion)
    }
}

//
//  DictionaryOfWeakItemsOnDemand.swift
//
//  Created by Jan Mazurczak on 23/08/2019.
//  Copyright © 2019 Jan's Games. All rights reserved.
//

import Foundation

public struct Weak<Item: AnyObject> {
    public weak var item: Item?
}

/// Returned items are weak so needed to be stored where needed and as long as needed.
public class WeakOnDemandDictionary<Identifier: Hashable, Item: AnyObject>: SyncDictionary<Identifier, Item, Weak<Item>> {
    
    public init() {
        super.init(itemPath: \.item, storageCreator: Weak.init)
    }
    
    internal override func sync(operation: @escaping () -> Void) {
        syncQueue.addOperation {
            self.dictionary = self.dictionary.filter { $0.value.item != nil }
            operation()
        }
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    
}

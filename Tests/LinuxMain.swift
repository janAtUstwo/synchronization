import XCTest

import SynchronizationTests

var tests = [XCTestCaseEntry]()
tests += SynchronizationTests.allTests()
XCTMain(tests)
